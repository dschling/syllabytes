/**
 *
 */
package edu.westga.syllabytes.views;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;

import net.fortuna.ical4j.data.ParserException;
import edu.westga.syllabytes.model.CourseSchedule;
import edu.westga.syllabytes.model.ICourseScheduleModel;
import edu.westga.syllabytes.model.LecturePeriod;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

/**
 * controller class for the SyllabytesMainView.fxml
 *
 * @author Dalton Schling
 * @version Fall 2015
 *
 */
public class SyllabytesMainViewController {

	private ICourseScheduleModel courseSchedule;
	private File repeatingEventsCalendar;

	@FXML
	private TableView<LecturePeriod> tableView;

	@FXML
	private TableColumn<LecturePeriod, String> titleColumn;

	@FXML
	private TableColumn<LecturePeriod, String> descriptionColumn;

	@FXML
	private TableColumn<LecturePeriod, LocalDate> dateColumn;

	@FXML
	private DatePicker datePicker;

	@FXML
	private CheckBox sundayCheckBox;
	@FXML
	private CheckBox mondayCheckBox;
	@FXML
	private CheckBox tuesdayCheckBox;
	@FXML
	private CheckBox wednesdayCheckBox;
	@FXML
	private CheckBox thursdayCheckBox;
	@FXML
	private CheckBox fridayCheckBox;
	@FXML
	private CheckBox saturdayCheckBox;

	@FXML
	void initialize() {
		assert this.tableView != null : "table view is null";
		assert this.titleColumn != null : "title column is null";
		assert this.descriptionColumn != null : "description column is null";
		assert this.dateColumn != null : "date column is null";
		assert this.datePicker != null : "Date picker is null";

		this.courseSchedule = new CourseSchedule();
		this.repeatingEventsCalendar = new File("src/edu/westga/syllabytes/model/tests/repeatingEvents.ics");
		this.initializeTableView();
	}

	/**
	 * binds table view and columns and loads in the calendar file.
	 */
	private void initializeTableView() {
		this.sundayCheckBox.selectedProperty().bindBidirectional(this.courseSchedule.sundayProperty());
		this.mondayCheckBox.selectedProperty().bindBidirectional(this.courseSchedule.mondayProperty());
		this.tuesdayCheckBox.selectedProperty().bindBidirectional(this.courseSchedule.tuesdayProperty());
		this.wednesdayCheckBox.selectedProperty().bindBidirectional(this.courseSchedule.wednesdayProperty());
		this.thursdayCheckBox.selectedProperty().bindBidirectional(this.courseSchedule.thursdayProperty());
		this.fridayCheckBox.selectedProperty().bindBidirectional(this.courseSchedule.fridayProperty());
		this.saturdayCheckBox.selectedProperty().bindBidirectional(this.courseSchedule.saturdayProperty());

		this.titleColumn.setCellValueFactory(cellData -> cellData.getValue().titleProperty());
		this.descriptionColumn.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
		this.dateColumn.setCellValueFactory(cellData -> cellData.getValue().dateProperty());

		this.tableView.setItems(this.courseSchedule.getSchedule());

		try {
			this.courseSchedule.loadFromFile(this.repeatingEventsCalendar);
		} catch (ParseException | IOException | ParserException exc) {
			exc.printStackTrace();
		}

	}

	/**
	 * Updates the dates in the course schedule and displays them in the table
	 * view.
	 */
	public void updateAllDates() {
		this.courseSchedule.updateAllDates();
		this.tableView.setItems(this.courseSchedule.getSchedule());
	}

	public void changeStartDate() {
		this.courseSchedule.changeStartDate(this.datePicker.getValue());
		this.updateAllDates();
	}

}
