package edu.westga.syllabytes.model;

import java.time.LocalDate;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * class corresponding to ICalendar VEVENT objects
 * @author Dalton Schling
 * @version Fall 2015
 *
 */
public class LecturePeriod {

	private StringProperty title;
	private StringProperty description;
	private ObjectProperty<LocalDate> date;
	private LocalDate startDate;

	/**
	 * Creates a new instance of a LecturePeriod
	 *
	 * @param title	Event Title
	 * @param description	Event Description
	 * @param date	Date of the event
	 */
	public LecturePeriod(String title, String description, LocalDate date) {
		this.title = new SimpleStringProperty(title);
		this.description = new SimpleStringProperty(description);
		this.date = new SimpleObjectProperty<LocalDate>(date);
		this.startDate = date;
	}

	/**
	 * Returns the lecture title.
	 * @return	the LecturePeriod title
	 */
	public StringProperty titleProperty() {
		return this.title;
	}

	/**
	 * Returns the lecture description.
	 * @return	the LecturePeriod description.
	 */
	public StringProperty descriptionProperty() {
		return this.description;
	}

	/** Returns the lecture date
	 * @return	the LecturePeriod date.
	 */
	public ObjectProperty<LocalDate> dateProperty() {
		return this.date;
	}

	/**
	 * Returns the original start date of this lecture period
	 * @return	the start date
	 */
	public LocalDate getStartDate() {
		return this.startDate;
	}

	public void setStartDate(LocalDate newDate) {
		this.startDate = newDate;
	}

}
