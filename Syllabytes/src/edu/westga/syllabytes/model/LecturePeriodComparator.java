/**
 *
 */
package edu.westga.syllabytes.model;

import java.util.Comparator;

/**
 * Comparator for LecturePeriod objects
 * @author Dalton Schling
 * @version	Fall 2015
 *
 */
public class LecturePeriodComparator implements Comparator<LecturePeriod> {

	@Override
	public int compare(LecturePeriod o1, LecturePeriod o2) {
		return o1.dateProperty().getValue().compareTo(o2.dateProperty().getValue());
	}

}
