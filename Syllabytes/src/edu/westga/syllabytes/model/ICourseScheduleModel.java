package edu.westga.syllabytes.model;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;

import net.fortuna.ical4j.data.ParserException;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableList;

/**
 * Interface for Course Schedule objects
 * @author dschl_000
 * @version Fall 2015
 *
 */
public interface ICourseScheduleModel {


	/**
	 * Accessor method for the start date
	 *
	 * @precondition CourseSchedule != empty
	 * @return	start of the semester
	 */
	ObjectProperty<LocalDate> getStartDate();

	/**
	 * Accessor method for the course schedule.
	 * @return	the course schedule
	 */
	ObservableList<LecturePeriod> getSchedule();

	/**
	 * Loads a file an iCalendar file into the course schedule.
	 * @param icalFile	calendar file
	 * @throws ParseException	parse exception
	 * @throws ParserException 	parser exception
	 * @throws IOException 	IO exception
	 */
	void loadFromFile(File icalFile) throws ParseException, IOException, ParserException;

	/**
	 * Changes the date of all LecturePeriod objects in the schedule
	 * based on the days of the week that the class meets.
	 *
	 * @precondition	schedule is not empty
	 * @param startDate	semester start date
	 */
	void updateAllDates();

	/**
	 * Changes the start date of the lecture.
	 * @param date	the date to change to
	 */
	void changeStartDate(LocalDate date);

	/**
	 *  True if the course meets on Sundays during the semester
	 * @return	BooleanProperty if the class meets on this day
	 */
	BooleanProperty sundayProperty();

	/**
	 *  True if the course meets on Mondays during the semester
	 * @return	BooleanProperty if the class meets on this day
	 */
	BooleanProperty mondayProperty();

	/**
	 *  True if the course meets on Tuesdays during the semester
	 * @return	BooleanProperty if the class meets on this day
	 */
	BooleanProperty tuesdayProperty();

	/**
	 *  True if the course meets on Wednesdays during the semester
	 * @return	BooleanProperty if the class meets on this day
	 */
	BooleanProperty wednesdayProperty();

	/**
	 *  True if the course meets on Thursdays during the semester
	 * @return	BooleanProperty if the class meets on this day
	 */
	BooleanProperty thursdayProperty();

	/**
	 *  True if the course meets on Fridays during the semester
	 * @return	BooleanProperty if the class meets on this day
	 */
	BooleanProperty fridayProperty();

	/**
	 *  True if the course meets on Saturdays during the semester
	 * @return	BooleanProperty if the class meets on this day
	 */
	BooleanProperty saturdayProperty();

}
