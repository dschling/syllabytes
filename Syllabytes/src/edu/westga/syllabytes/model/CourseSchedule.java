package edu.westga.syllabytes.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ModifiableObservableListBase;
import javafx.collections.ObservableList;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.ComponentList;
import net.fortuna.ical4j.model.Date;
import net.fortuna.ical4j.model.DateList;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.parameter.Value;
import net.fortuna.ical4j.model.property.RRule;

/**
 * class corresponding to ICalendar VCALENDAR objects
 *
 * @author Dalton Schling
 * @version Fall 2015
 *
 */
public class CourseSchedule extends ModifiableObservableListBase<LecturePeriod> implements ICourseScheduleModel {

	private static final String PERIOD_START = "20150101";
	private static final String PERIOD_END = "20170101";
	private ArrayList<LecturePeriod> schedule;
	private Calendar calendar;
	private ObjectProperty<LocalDate> startDate;

	private BooleanProperty onSunday;
	private BooleanProperty onMonday;
	private BooleanProperty onTuesday;
	private BooleanProperty onWednesday;
	private BooleanProperty onThursday;
	private BooleanProperty onFriday;
	private BooleanProperty onSaturday;

	private HashMap<DayOfWeek, BooleanProperty> daysOfWeekMap;

	/**
	 * Creates an instance of CourseSchedule.
	 */
	public CourseSchedule() {
		this.schedule = new ArrayList<LecturePeriod>();
		this.startDate = new SimpleObjectProperty<LocalDate>();
		this.daysOfWeekMap = new HashMap<>();

		this.onSunday = new SimpleBooleanProperty();
		this.onMonday = new SimpleBooleanProperty();
		this.onTuesday = new SimpleBooleanProperty();
		this.onWednesday = new SimpleBooleanProperty();
		this.onThursday = new SimpleBooleanProperty();
		this.onFriday = new SimpleBooleanProperty();
		this.onSaturday = new SimpleBooleanProperty();

		this.daysOfWeekMap.put(DayOfWeek.SUNDAY, this.onSunday);
		this.daysOfWeekMap.put(DayOfWeek.MONDAY, this.onMonday);
		this.daysOfWeekMap.put(DayOfWeek.TUESDAY, this.onTuesday);
		this.daysOfWeekMap.put(DayOfWeek.WEDNESDAY, this.onWednesday);
		this.daysOfWeekMap.put(DayOfWeek.THURSDAY, this.onThursday);
		this.daysOfWeekMap.put(DayOfWeek.FRIDAY, this.onFriday);
		this.daysOfWeekMap.put(DayOfWeek.SATURDAY, this.onSaturday);

	}

	/**
	 * Given an iCalendar file, populates the CourseSchedule with LecturePeriod
	 * objects based on the VEVENT's in the iCalendar file.
	 *
	 * @param iCalFile
	 *            file to load.
	 * @throws ParseException
	 *             parse exception
	 * @throws ParserException
	 *             parser exception
	 * @throws IOException
	 *             IO exception
	 */
	public void loadFromFile(File iCalFile) throws ParseException, IOException, ParserException {

		if (iCalFile == null) {
			throw new IllegalArgumentException("File is null");
		}

		this.buildCalendar(iCalFile);
		this.addEventsToSchedule();
		this.schedule.sort(new LecturePeriodComparator());
		if (this.schedule.size() > 0) {
			this.startDate.setValue(this.schedule.get(0).dateProperty().getValue());
		}
	}

	/**
	 * Adds events from calendar to the CourseSchedule as LecturePeriods.
	 *
	 * @throws ParseException
	 *             parse exception
	 */
	@SuppressWarnings("unchecked")
	private void addEventsToSchedule() throws ParseException {
		ComponentList components = this.calendar.getComponents(Component.VEVENT);
		Iterator<VEvent> iterator = components.iterator();
		while (iterator.hasNext()) {
			VEvent vEvent = (VEvent) iterator.next();
			String name = vEvent.getSummary().getValue();
			String description = vEvent.getDescription().getValue();
			LocalDate date;

			RRule rRule = (RRule) vEvent.getProperty(Property.RRULE);
			if (rRule != null) {
				DateList dates = null;

				dates = rRule.getRecur().getDates(new Date(PERIOD_START), new Date(PERIOD_END), Value.DATE);

				Iterator<Date> dateIterator = dates.iterator();
				while (dateIterator.hasNext()) {
					date = dateIterator.next().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					this.add(new LecturePeriod(name, description, date));
				}
			} else {
				date = vEvent.getStartDate().getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				this.add(new LecturePeriod(name, description, date));
			}
		}
	}

	/**
	 * Builds a calendar from a file
	 *
	 * @param iCalFile
	 *            file to load
	 * @throws ParserException
	 *             parser exception
	 * @throws IOException
	 *             IO exception
	 */
	private void buildCalendar(File iCalFile) throws IOException, ParserException {
		FileInputStream inputStream = null;

		inputStream = new FileInputStream(iCalFile);

		CalendarBuilder builder = new CalendarBuilder();

		this.calendar = builder.build(inputStream);
	}

	/**
	 * Getter for the course schedule
	 *
	 * @return the schedule of LecturePeriods
	 */
	public ObservableList<LecturePeriod> getSchedule() {
		return this;
	}

	@Override
	protected void doAdd(int index, LecturePeriod element) {
		this.schedule.add(index, element);
	}

	@Override
	protected LecturePeriod doRemove(int index) {
		return this.schedule.remove(index);
	}

	@Override
	protected LecturePeriod doSet(int index, LecturePeriod element) {
		return this.schedule.set(index, element);
	}

	@Override
	public LecturePeriod get(int index) {
		return this.schedule.get(index);
	}

	@Override
	public int size() {
		return this.schedule.size();
	}

	@Override
	public ObjectProperty<LocalDate> getStartDate() {
		if (this.schedule.size() == 0) {
			throw new IllegalStateException("Can not get start date from empty schedule");
		}
		return this.startDate;
	}

	@Override
	public void updateAllDates() {
		if (this.schedule.size() == 0) {
			throw new IllegalStateException("The schedule is empty");
		}
		LocalDate newDate;
		for (LecturePeriod lecturePeriod : this.schedule) {
			newDate = this.calculateNextDate(lecturePeriod.getStartDate());
			lecturePeriod.dateProperty().setValue(newDate);
		}
	}

	/**
	 * Given a LocalDate to start with, this method determines the first date on
	 * or after it that falls on one of the days-of-the-week in which the course
	 * meets.
	 *
	 * @param date
	 *            date to calculate next date from
	 * @return LocalDate the next date that falls on a day the class meets
	 */
	public LocalDate calculateNextDate(LocalDate date) {
		LocalDate nextDate = date;
		BooleanProperty classMeets = this.daysOfWeekMap.get(nextDate.getDayOfWeek());
		if (classMeets.getValue()) {
			return nextDate;
		}
		while (!classMeets.getValue()) {
			nextDate = nextDate.plusDays(1);
			classMeets = this.daysOfWeekMap.get(nextDate.getDayOfWeek());
		}
		return nextDate;
	}

	@Override
	public BooleanProperty sundayProperty() {
		return this.onSunday;
	}

	@Override
	public BooleanProperty mondayProperty() {
		return this.onMonday;
	}

	@Override
	public BooleanProperty tuesdayProperty() {
		return this.onTuesday;
	}

	@Override
	public BooleanProperty wednesdayProperty() {
		return this.onWednesday;
	}

	@Override
	public BooleanProperty thursdayProperty() {
		return this.onThursday;
	}

	@Override
	public BooleanProperty fridayProperty() {
		return this.onFriday;
	}

	@Override
	public BooleanProperty saturdayProperty() {
		return this.onSaturday;
	}

	@Override
	public void changeStartDate(LocalDate date) {
		if (this.schedule.size() == 0) {
			throw new IllegalStateException("The schedule is empty");
		}
		LocalDate startDate = date;
		for (LecturePeriod lecturePeriod : this.schedule) {
			lecturePeriod.setStartDate(startDate);
			startDate = date.plusDays(1);
		}
	}

}
