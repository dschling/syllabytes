package edu.westga.syllabytes.model.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import edu.westga.syllabytes.model.CourseSchedule;

/**
 * Test case for calculating next date.
 * @author dschl_000
 * @version	Fall 2015
 *
 */
public class WhenCalculatingNextDate {

	private static final LocalDate START_DATE = LocalDate.of(2016, 1, 10);
	private CourseSchedule courseSchedule;
	private File noRepeatingEventsCalendar;
	private File noEventsCalendar;

	/**
	 * set up method
	 */
	@Before
	public void setUp() {

	}

	/**
	 * test case for calculateNextDate
	 */
	@Test
	public void shouldBeNormalIfNoneChecked() {
		fail("Not yet implemented");
	}

	/**
	 * test case for calculateNextDate
	 */
	@Test
	public void shouldHaveAllMondayClasses() {
		fail("Not yet implemented");
	}

	/**
	 * test case for calculateNextDate
	 */
	@Test
	public void shouldHaveMondayWednesdayClasses() {
		fail("Not yet implemented");
	}

	/**
	 * test case for calculateNextDate
	 */
	@Test
	public void shouldHaveMondayWednesdayFridayClasses() {
		fail("Not yet implemented");
	}

	/**
	 * test case for calculateNextDate
	 */
	@Test
	public void ShouldHaveClassesEveryDayOfWeek() {
		fail("Not yet implemented");
	}



}
