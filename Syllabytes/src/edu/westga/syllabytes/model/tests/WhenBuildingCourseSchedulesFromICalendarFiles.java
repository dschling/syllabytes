package edu.westga.syllabytes.model.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import javafx.collections.ObservableList;
import net.fortuna.ical4j.data.ParserException;

import org.junit.Before;
import org.junit.Test;

import edu.westga.syllabytes.model.CourseSchedule;
import edu.westga.syllabytes.model.LecturePeriod;

/**
 * Unit test for loadFromFile
 * @author dschl_000
 * @version Fall 2015
 *
 */
public class WhenBuildingCourseSchedulesFromICalendarFiles {

	private File repeatingEventsCalendar;
	private File noRepeatingEventsCalendar;
	private CourseSchedule courseSchedule;
	private File noEventsCalendar;

	/**
	 * no-op constructor
	 */
	public WhenBuildingCourseSchedulesFromICalendarFiles() {
	}

	/**
	 * set up method
	 */
	@Before
	public void setup() {
		this.repeatingEventsCalendar = new File("src/edu/westga/syllabytes/model/tests/repeatingEvents.ics");
		this.noRepeatingEventsCalendar = new File("src/edu/westga/syllabytes/model/tests/noRepeatingEvents.ics");
		this.noEventsCalendar = new File("src/edu/westga/syllabytes/model/tests/noEvents.ics");
		this.courseSchedule = new CourseSchedule();
	}


	/**
	 * Test method
	 * @throws ParseException	parse exception
	 * @throws IOException	IO exception
	 * @throws ParserException	parser exception
	 */
	@Test(expected = IllegalArgumentException.class)
	public void nullFileShouldThrowException() throws ParseException, IOException, ParserException {
		this.courseSchedule.loadFromFile(null);
	}

	/**
	 * Test method
	 * @throws ParseException	parse exception
	 * @throws IOException	IO exception
	 * @throws ParserException	parser exception
	 */
	@Test
	public void shouldHaveEmptySchedule() throws ParseException, IOException, ParserException {
		this.courseSchedule.loadFromFile(this.noEventsCalendar);
	}

	/**
	 * Test method
	 * @throws ParseException	parse exception
	 * @throws IOException	IO exception
	 * @throws ParserException	parser exception
	 */
	@Test
	public void shouldHave4EventsAfterLoadingNonRecurringEvents() throws ParseException, IOException, ParserException {
		this.courseSchedule.loadFromFile(this.noRepeatingEventsCalendar);
	}


	/**
	 * Test method
	 * @throws ParseException	parse exception
	 * @throws IOException	IO exception
	 * @throws ParserException	parser exception
	 */
	@Test
	public void shouldHave7EventsInScheduleAfterLoadingRecurringEvents() throws ParseException, IOException, ParserException {
		this.courseSchedule.loadFromFile(this.repeatingEventsCalendar);
		ObservableList<LecturePeriod> schedule = this.courseSchedule.getSchedule();
		assertEquals(7, schedule.size());
	}

	/**
	 * Test method
	 * @throws ParseException	parse exception
	 * @throws IOException	IO exception
	 * @throws ParserException	parser exception
	 */
	@Test
	public void shouldHave11EventsInScheduleAfterLoadingAllCalendars() throws ParseException, IOException, ParserException {
		this.courseSchedule.loadFromFile(this.repeatingEventsCalendar);
		this.courseSchedule.loadFromFile(this.noRepeatingEventsCalendar);
		this.courseSchedule.loadFromFile(this.noEventsCalendar);
		ObservableList<LecturePeriod> schedule = this.courseSchedule.getSchedule();
		assertEquals(11, schedule.size());
	}

	/**
	 * Test method
	 * @throws ParseException	parse exception
	 * @throws IOException	IO exception
	 * @throws ParserException	parser exception
	 */
	@Test
	public void lecturePeriodsShouldBeInOrderByDate() throws ParseException, IOException, ParserException {
		this.courseSchedule.loadFromFile(this.noRepeatingEventsCalendar);
		ObservableList<LecturePeriod> schedule = this.courseSchedule.getSchedule();
		assertTrue(schedule.get(0).titleProperty().getValue().equals("Robotics class"));
		assertTrue(schedule.get(1).titleProperty().getValue().equals("Software Engineering"));
		assertTrue(schedule.get(2).titleProperty().getValue().equals("Math Class"));
		assertTrue(schedule.get(3).titleProperty().getValue().equals("Program Construction"));
	}





}
