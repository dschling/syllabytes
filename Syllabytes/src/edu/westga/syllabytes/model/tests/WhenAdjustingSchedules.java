/**
 *
 */
package edu.westga.syllabytes.model.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;

import net.fortuna.ical4j.data.ParserException;

import org.junit.Before;
import org.junit.Test;

import edu.westga.syllabytes.model.CourseSchedule;

/**
 * Unit tests for updateAllDates() in CourseSchedule
 *
 * @author dschl_000
 * @version Fall 2015
 *
 */
public class WhenAdjustingSchedules {

	private static final LocalDate START_DATE = LocalDate.of(2016, 1, 10);
	private CourseSchedule courseSchedule;
	private File noRepeatingEventsCalendar;
	private File noEventsCalendar;

	/**
	 * Setup method.
	 */
	@Before
	public void setUp() {
		this.noRepeatingEventsCalendar = new File("src/edu/westga/syllabytes/model/tests/noRepeatingEvents.ics");
		this.noEventsCalendar = new File("src/edu/westga/syllabytes/model/tests/noEvents.ics");
		this.courseSchedule = new CourseSchedule();
	}

	/**
	 * Test method for
	 * {@link edu.westga.syllabytes.model.CourseSchedule#updateAllDates(java.time.LocalDate)}
	 * .
	 * @throws ParseException	parse exception
	 * @throws ParserException 	parser exception
	 * @throws IOException 	IO exception
	 */
	@Test
	public void firstDateShouldEqualStartDate() throws ParseException, IOException, ParserException {
		this.courseSchedule.loadFromFile(this.noRepeatingEventsCalendar);
		//this.courseSchedule.updateAllDates(START_DATE);
		LocalDate firstEventDate = this.courseSchedule.get(0).dateProperty().getValue();
		assertTrue(firstEventDate.isEqual(START_DATE));
	}

	/**
	 * Test method for
	 * {@link edu.westga.syllabytes.model.CourseSchedule#updateAllDates(java.time.LocalDate)}
	 * .
	 * @throws ParseException	parse exception
	 * @throws ParserException 	parser exception
	 * @throws IOException 	IO exception
	 */
	@Test
	public void lastDateShouldEqualStartDatePlus3() throws ParseException, IOException, ParserException {
		this.courseSchedule.loadFromFile(this.noRepeatingEventsCalendar);
		//this.courseSchedule.updateAllDates(START_DATE);
		LocalDate lastEventDate = this.courseSchedule.get(3).dateProperty().getValue();
		assertTrue(lastEventDate.isEqual(START_DATE.plusDays(3)));
	}

	/**
	 * Test method for
	 * {@link edu.westga.syllabytes.model.CourseSchedule#updateAllDates(java.time.LocalDate)}
	 * .
	 * @throws ParseException	parse exception
	 * @throws ParserException 	parser exception
	 * @throws IOException 	IO exception
	 */
	@Test
	public void middleDateShouldEqualStartDatePlus2() throws ParseException, IOException, ParserException {
		this.courseSchedule.loadFromFile(this.noRepeatingEventsCalendar);
		//this.courseSchedule.updateAllDates(START_DATE);
		LocalDate middleEventDate = this.courseSchedule.get(2).dateProperty().getValue();
		assertTrue(middleEventDate.isEqual(START_DATE.plusDays(2)));
	}

	/**
	 * Test method for
	 * {@link edu.westga.syllabytes.model.CourseSchedule#updateAllDates(java.time.LocalDate)}
	 * .
	 * @throws ParseException	parse exception
	 * @throws ParserException 	parser exception
	 * @throws IOException 	IO exception
	 */
	@Test(expected = IllegalStateException.class)
	public void updateEmptyScheduleShouldThrowException() throws ParseException, IOException, ParserException {
		this.courseSchedule.loadFromFile(this.noEventsCalendar);
		//this.courseSchedule.updateAllDates(START_DATE);
	}

	/**
	 * Test method for
	 * {@link edu.westga.syllabytes.model.CourseSchedule#updateAllDates(java.time.LocalDate)}
	 * .
	 * @throws ParseException	parse exception
	 * @throws ParserException 	parser exception
	 * @throws IOException 	IO exception
	 */
	@Test
	public void shouldUpdateToLastTimeCalled() throws ParseException, IOException, ParserException {
		this.courseSchedule.loadFromFile(this.noRepeatingEventsCalendar);

		LocalDate aDate = LocalDate.of(2014, 1, 1);
		//this.courseSchedule.updateAllDates(aDate);
		aDate = LocalDate.of(2015, 1, 1);
		//this.courseSchedule.updateAllDates(aDate);
		//this.courseSchedule.updateAllDates(START_DATE);

		LocalDate firstEventDate = this.courseSchedule.get(0).dateProperty().getValue();
		assertTrue(firstEventDate.isEqual(START_DATE));

		LocalDate middleEventDate = this.courseSchedule.get(2).dateProperty().getValue();
		assertTrue(middleEventDate.isEqual(START_DATE.plusDays(2)));

		LocalDate lastEventDate = this.courseSchedule.get(3).dateProperty().getValue();
		assertTrue(lastEventDate.isEqual(START_DATE.plusDays(3)));
	}

}
