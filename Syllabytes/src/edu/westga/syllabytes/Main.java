/**
 *
 */
package edu.westga.syllabytes;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Loads and starts the application
 * @author dschl_000
 * @version	Fall 2015
 *
 */
public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		URL resource = classLoader.getResource("edu/westga/syllabytes/views/fxml/SyllabytesMainView.fxml");
		FXMLLoader loader = new FXMLLoader(resource);
		Parent root = (Parent) loader.load();
		Scene scene = new Scene(root);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	/**
	 * Starts the program
	 * @param args	args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
